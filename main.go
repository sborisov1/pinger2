package main

import (
    "os/exec"
	"strings"
	"fmt"
	"net"
	"time"
)

func ping(Adr string) (string) {
	
	out, _ := exec.Command("ping", Adr, "-w", "10", "-n", "1").Output()
	//fmt.Println(string(out))
	status := "undifenied"
	if strings.Contains(string(out), "=32") {
		status = "ONLINE"
		fmt.Println("IT'S ALIVEEE")
	} else {
		status = "OFFLINE"
		fmt.Println("TANGO DOWN")
	}
	return status
}

func getInterfaceIpv4Addr(interfaceName string) (addr string, err error) {
    var (
        ief      *net.Interface
        addrs    []net.Addr
        ipv4Addr net.IP
    )
    if ief, err = net.InterfaceByName(interfaceName); err != nil { // get interface
        return
    }
    if addrs, err = ief.Addrs(); err != nil { // get addresses
        return
    }
    for _, addr := range addrs { // get ipv4 address
        if ipv4Addr = addr.(*net.IPNet).IP.To4(); ipv4Addr != nil {
            break
        }
    }
    if ipv4Addr == nil {
        return "", fmt.Errorf("interface %s don't have an ipv4 address", interfaceName)
    }
    return ipv4Addr.String(), nil
}

func main() {

	// MAIN SERVER MAIN address is 192.168.88.1
	// SLAVE SERVER MAIN address is 192.168.88.2
	// MAIN SERVER SERVICE address is 192.168.0.1
	// SLAVE SERVER SERVICE address is 192.168.0.2
	// SLAVE SERVER MAIN ADAPTER is "Ethernet 4" 

	for {
		
		currentIP, _ := getInterfaceIpv4Addr("NIC1") // SLAVE SERVER IPv4 address (MAIN address) via MAIN adapter
		fmt.Println(currentIP)
		
		remoteCompPing := ping("192.168.0.1") // ping MAIN SERVER via SERVICE adapter
		if remoteCompPing == "OFFLINE" {
			if currentIP != "192.168.88.1" {
				out, _ := exec.Command("netsh", "interface", "ip", "set", "address", "name="+"NIC1", "static", "192.168.88.1", "255.255.255.0").Output()
				fmt.Println(string(out))
			}
		}
		if remoteCompPing == "ONLINE" {
			if currentIP != "192.168.88.2" {
				fmt.Println("changing")
				out, _ := exec.Command("netsh", "interface", "ip", "set", "address", "name="+"NIC1", "static", "192.168.88.2", "255.255.255.0").Output()
				fmt.Println(string(out))
			}
		}
		
		time.Sleep(5 * time.Second)
	}
}